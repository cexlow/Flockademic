// react-ga attempts to modify the DOM to add a <script> tag, so skip that
jest.mock('react-ga');

import App from '../src/app';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

it('should match the snapshot', () => {
  const app = shallow(<App/>);

  expect(toJson(app)).toMatchSnapshot();
});
